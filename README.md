# Segmenter Transformer Models for Semantic Segmentation

Project adjusted from original repository https://github.com/rstrudel/segmenter according to needs of the research. Implemented in Python using PyTorch library.

Researching, training and redesigning these Segmenter transformer models for semantic segmentation is part of my grad thesis deep learning project, which has the goal of comparing state-of-the-art transformer to convolutional models.

Training Segmenter models and measuring their memory/speed performance.

Creating, implementing, training and evaluating transformer Segmenter-Segmenter pyramidal models on more levels of image resolution pyramid (branch "segpyr") with option of shared decoder which recombines features given as output of Segmenter encoders (branch "segpyrdec").

Project duration: Feb 2022 - July 2022.
