import torch
import torch.nn as nn
import torch.nn.functional as F

from segm.model.utils import padding, unpadding
from timm.models.layers import trunc_normal_


class Segmenter(nn.Module):
    def __init__(
        self,
        encoder,
        decoder,
        n_cls,
    ):
        super().__init__()
        self.n_cls = n_cls
        self.patch_size = encoder.patch_size
        self.encoder = encoder
        self.decoder = decoder

    @torch.jit.ignore
    def no_weight_decay(self):
        def append_prefix_no_weight_decay(prefix, module):
            return set(map(lambda x: prefix + x, module.no_weight_decay()))

        nwd_params = append_prefix_no_weight_decay("encoder.", self.encoder).union(
            append_prefix_no_weight_decay("decoder.", self.decoder)
        )
        return nwd_params

    def forward(self, im):
        H_ori, W_ori = im.size(2), im.size(3)
        im = padding(im, self.patch_size)
        H, W = im.size(2), im.size(3)

        #print("IM====",im.shape)
        x = self.encoder(im, return_features=True)

        # remove CLS/DIST tokens for decoding
        num_extra_tokens = 1 + self.encoder.distilled
        x = x[:, num_extra_tokens:]

        masks = self.decoder(x, (H, W))

        masks = F.interpolate(masks, size=(H, W), mode="bilinear")
        masks = unpadding(masks, (H_ori, W_ori))

        return masks

    def get_attention_map_enc(self, im, layer_id, vis_joint_attn=False, vis_joint_attn_all_layers=False):
        import numpy
        print("00im",im.shape,im.min(),im.max())
        attn_map = self.encoder.get_attention_map(im, layer_id, vis_joint_attn=vis_joint_attn)
        print("11im",im.shape)
        if not vis_joint_attn:
            return attn_map
        attn_map = torch.stack(attn_map).squeeze(1)
        print("ATMAP",attn_map.shape)
        # avg attn weights over heads
        attn_map = torch.mean(attn_map, dim=1)

        res_id_mat = torch.eye(attn_map.shape[1]).to(attn_map.device)
        attn_map += res_id_mat
        attn_map /= torch.sum(attn_map, dim=-1).unsqueeze(-1)
        
        joint_attns = torch.zeros_like(attn_map)
        print("JA",joint_attns.shape)
        joint_attns[0] = attn_map[0]
        for i in range(1, len(joint_attns)):
            joint_attns[i] = attn_map[i] @ joint_attns[i-1]
        print("joint_attentions...",len(joint_attns), joint_attns.shape)

        num_extra_tokens = 1 + self.encoder.distilled
        if vis_joint_attn_all_layers:
            res_list = []
            num_lays = self.encoder.n_layers
            for lay in range(num_lays):
                at_vis_mask = joint_attns[lay, 0, num_extra_tokens:]
                print("MASK",torch.min(at_vis_mask),torch.max(at_vis_mask))
                print("IM====",im.shape,im.dtype,im.device,max(im.shape[-2],im.shape[-1])//min(im.shape[-2],im.shape[-1]))

                # norm
                at_vis_mask /= at_vis_mask.max()
                print("11111 AT",at_vis_mask.shape,torch.min(at_vis_mask),torch.max(at_vis_mask))
                im_h, im_w = im.shape[-2:]
                hw_ratio = max(im_h,im_w)//min(im_h,im_w)

                from math import sqrt
                shorter_dim = int(sqrt(at_vis_mask.shape[0] // hw_ratio))
                if im_h < im_w:
                    vis_mask = at_vis_mask.reshape((1,1,shorter_dim,shorter_dim*hw_ratio))
                else:
                    vis_mask = at_vis_mask.reshape((1,1,shorter_dim*hw_ratio,shorter_dim))
                print("mask after=",vis_mask.shape,vis_mask.dtype,torch.min(vis_mask),torch.max(vis_mask))

                attentions = F.interpolate(vis_mask, scale_factor=self.patch_size, mode="nearest")#.cpu().numpy()
                print("///////////",attentions.shape,torch.min(attentions),torch.max(attentions))

                res = im * attentions
                res = res[0].cpu()
                #res = res[0].reshape(im_h,im_w,3).cpu().numpy()
                print("RES", res.shape,res.dtype)

                res_list.append(res)
            return res_list

        at_vis_mask = joint_attns[-1, 0, num_extra_tokens:]
        print("MASK", torch.min(at_vis_mask), torch.max(at_vis_mask))
        print("IM====", im.shape, im.dtype, im.device,
              max(im.shape[-2], im.shape[-1]) // min(im.shape[-2], im.shape[-1]))

        # norm
        at_vis_mask /= at_vis_mask.max()
        print("11111 AT", at_vis_mask.shape, torch.min(at_vis_mask), torch.max(at_vis_mask))
        im_h, im_w = im.shape[-2:]
        hw_ratio = max(im_h, im_w) // min(im_h, im_w)

        from math import sqrt
        shorter_dim = int(sqrt(at_vis_mask.shape[0] // hw_ratio))
        if im_h < im_w:
            vis_mask = at_vis_mask.reshape((1, 1, shorter_dim, shorter_dim * hw_ratio))
        else:
            vis_mask = at_vis_mask.reshape((1, 1, shorter_dim * hw_ratio, shorter_dim))
        print("mask after=", vis_mask.shape, vis_mask.dtype, torch.min(vis_mask), torch.max(vis_mask))

        attentions = F.interpolate(vis_mask, scale_factor=self.patch_size, mode="nearest")  # .cpu().numpy()
        print("///////////", attentions.shape, torch.min(attentions), torch.max(attentions))

        res = im * attentions
        res = res[0].cpu()
        # res = res[0].reshape(im_h,im_w,3).cpu().numpy()
        print("RES", res.shape, res.dtype)

        return res

    def get_attention_map_dec(self, im, layer_id, vis_joint_attn=False, vis_joint_attn_all_layers=False):
        x = self.encoder(im, return_features=True)

        # remove CLS/DIST tokens for decoding
        num_extra_tokens = 1 + self.encoder.distilled
        x = x[:, num_extra_tokens:]

        attn_map = self.decoder.get_attention_map(x, layer_id, vis_joint_attn=vis_joint_attn)
        if not vis_joint_attn:
            return attn_map

        ##############
        attn_map = torch.stack(attn_map).squeeze(1)
        print("ATMAP", attn_map.shape)
        # avg attn weights over heads
        attn_map = torch.mean(attn_map, dim=1)

        res_id_mat = torch.eye(attn_map.shape[1]).to(attn_map.device)
        attn_map += res_id_mat
        attn_map /= torch.sum(attn_map, dim=-1).unsqueeze(-1)

        joint_attns = torch.zeros_like(attn_map)
        print("JA", joint_attns.shape)
        joint_attns[0] = attn_map[0]
        for i in range(1, len(joint_attns)):
            joint_attns[i] = attn_map[i] @ joint_attns[i - 1]
        print("joint_attentions...", len(joint_attns), joint_attns.shape)

        at_vis_mask = joint_attns[-1, -self.n_cls:, :-self.n_cls] ######## !!!!!!!!!
        print("MASK", torch.min(at_vis_mask), torch.max(at_vis_mask))
        print("IM====", im.shape, im.dtype, im.device,
              max(im.shape[-2], im.shape[-1]) // min(im.shape[-2], im.shape[-1]))

        # norm
        at_vis_mask /= at_vis_mask.max()
        print("11111 AT", at_vis_mask.shape, torch.min(at_vis_mask), torch.max(at_vis_mask))
        im_h, im_w = im.shape[-2:]
        hw_ratio = max(im_h, im_w) // min(im_h, im_w)

        from math import sqrt

        cls_res = []
        for i,at_vis_mask_cls in enumerate(at_vis_mask):
            print("at_vis_mask_cls",i,at_vis_mask_cls.shape)
            shorter_dim = int(sqrt(at_vis_mask_cls.shape[0] // hw_ratio))
            if im_h < im_w:
                vis_mask = at_vis_mask_cls.reshape((1, 1, shorter_dim, shorter_dim * hw_ratio))
            else:
                vis_mask = at_vis_mask_cls.reshape((1, 1, shorter_dim * hw_ratio, shorter_dim))
            print("mask after=", vis_mask.shape, vis_mask.dtype, torch.min(vis_mask), torch.max(vis_mask))

            attentions = F.interpolate(vis_mask, scale_factor=self.patch_size, mode="nearest")  # .cpu().numpy()
            print("///////////", attentions.shape, torch.min(attentions), torch.max(attentions))

            res = attentions #im * attentions
            res = res[0].cpu()
            # res = res[0].reshape(im_h,im_w,3).cpu().numpy()
            print("RES", res.shape, res.dtype)

            cls_res.append((i,res))

        return cls_res

