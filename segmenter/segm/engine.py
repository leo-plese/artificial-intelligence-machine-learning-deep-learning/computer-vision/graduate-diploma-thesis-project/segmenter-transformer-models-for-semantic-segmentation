import torch
import math

from segm.utils.logger import MetricLogger
from segm.metrics import gather_data, compute_metrics
from segm.model import utils
from segm.data.utils import IGNORE_LABEL
import segm.utils.torch as ptu
from segm.model.utils import inference #inference_one_batch


def train_one_epoch(
    model,
    data_loader,
    optimizer,
    lr_scheduler,
    epoch,
    amp_autocast,
    loss_scaler,
):
    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)
    logger = MetricLogger(delimiter="  ")
    header = f"Epoch: [{epoch}]"
    print_freq = 100 #1

    model.train()

    data_loader.set_epoch(epoch)
    num_updates = epoch * len(data_loader)
    for batch in logger.log_every(data_loader, print_freq, header):
        im = batch["im"].to(ptu.device)
        seg_gt = batch["segmentation"].long().to(ptu.device)
        with amp_autocast():
            #print("im====",im.shape,im.dtype)
            seg_pred = model.forward(im)
            loss = criterion(seg_pred, seg_gt)

        loss_value = loss.item()
        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value), force=True)

        optimizer.zero_grad()
        if loss_scaler is not None:
            loss_scaler(
                loss,
                optimizer,
                parameters=model.parameters(),
            )
        else:
            loss.backward()
            optimizer.step()

        num_updates += 1
        lr_scheduler.step_update(num_updates=num_updates)

        torch.cuda.synchronize()

        logger.update(
            loss=loss.item(),
            learning_rate=optimizer.param_groups[0]["lr"],
        )

    return logger

def train_one_epoch_duration(
    model,
    data_loader,
    optimizer,
    amp_autocast,
    loss_scaler,
):
    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)

    for i, batch in enumerate(data_loader):
        if i % 50 == 0:
            print("i", i)
        im = batch["im"].to(ptu.device)
        seg_gt = batch["segmentation"].long().to(ptu.device)

        torch.cuda.empty_cache()
        optimizer.zero_grad()

        with amp_autocast():
            seg_pred = model.forward(im)
            loss = criterion(seg_pred, seg_gt)

        if loss_scaler is not None:
            loss_scaler(
                loss,
                optimizer,
                parameters=model.parameters(),
            )
        else:
            loss.backward()
            optimizer.step()

        del seg_pred
        del loss
        torch.cuda.synchronize()


@torch.no_grad()
def evaluate(
    model,
    data_loader,
    val_seg_gt,
    window_size,
    window_stride,
    amp_autocast,
):
    model_without_ddp = model
    if hasattr(model, "module"):
        model_without_ddp = model.module
    logger = MetricLogger(delimiter="  ")
    header = "Eval:"
    print_freq = 50

    val_seg_pred = {}
    model.eval()
    for batch in logger.log_every(data_loader, print_freq, header):
        ims = [im.to(ptu.device) for im in batch["im"]]
        ims_metas = batch["im_metas"]
        ori_shape = ims_metas[0]["ori_shape"]
        ori_shape = (ori_shape[0].item(), ori_shape[1].item())
        filename = batch["im_metas"][0]["ori_filename"][0]

        with amp_autocast():
            seg_pred = utils.inference(
                model_without_ddp,
                ims,
                ims_metas,
                ori_shape,
                window_size,
                window_stride,
                batch_size=1,
            )
            seg_pred = seg_pred.argmax(0)

        seg_pred = seg_pred.cpu().numpy()
        val_seg_pred[filename] = seg_pred

    val_seg_pred = gather_data(val_seg_pred) if ptu.world_size > 1 else val_seg_pred
    scores = compute_metrics(
        val_seg_pred,
        val_seg_gt,
        data_loader.unwrapped.n_cls,
        ignore_index=IGNORE_LABEL,
        distributed=ptu.distributed,
    )

    for k, v in scores.items():
        logger.update(**{f"{k}": v, "n": 1})

    return logger

# LEO - BEGIN
##### FOR .forward (miou.py)
# def do_eval(
#     model,
#     bs,
#     crop_size,
#     im=None,
# ):
#     if im is None:
#         torch.cuda.empty_cache()
#         im = torch.randn(bs, 3, crop_size, crop_size).cuda(ptu.device)
#
#     with torch.no_grad():
#         model.forward(im)
#         #seg_pred = model.forward(im)
#         #y = torch.softmax(seg_pred, 1)
#         #del seg_pred
#         #del y
#
# def get_max_batchsize_eval(
#     model,
#     crop_size,
# ):
#     model.eval()
#
#     cur_bs = 2
#     while True:
#         try:
#             print("cur_bs =", cur_bs)
#             do_eval(
#                 model,
#                 cur_bs,
#                 crop_size,
#             )
#
#             cur_bs *= 2
#         except RuntimeError as err:
#             print("!!! err =", err)
#             top_bs = cur_bs
#             bottom_bs = cur_bs // 2
#             break
#
#     print("bottom =", bottom_bs)
#     print("top =", top_bs)
#     while (top_bs - bottom_bs) > 1:
#         mid_bs = (top_bs + bottom_bs) // 2
#         print("mid_bs =", mid_bs)
#         try:
#             do_eval(
#                 model,
#                 mid_bs,
#                 crop_size,
#             )
#             bottom_bs = mid_bs
#         except RuntimeError:
#             top_bs = mid_bs
#
#     print("bottom =", bottom_bs)
#     print("top =", top_bs)
#     try:
#         do_eval(
#             model,
#             top_bs,
#             crop_size,
#         )
#         max_bs = top_bs
#     except RuntimeError as err:
#         print("!!! err =", err)
#         max_bs = bottom_bs
#
#     return max_bs
#
# def measure_speed_eval_fps(model, bs, crop_size, n=30):
#     import torch
#     import time
#
#     model.eval()
#
#     warm_up = 10
#     torch.cuda.empty_cache()
#     im = torch.randn(bs, 3, crop_size, crop_size).cuda(ptu.device)
#
#     torch.cuda.synchronize()
#     with torch.no_grad():
#         for i in range(warm_up):
#             torch.cuda.empty_cache()
#             do_eval(
#                 model,
#                 bs,
#                 crop_size,
#                 im=im
#             )
#
#     start = time.time()
#     with torch.no_grad():
#         for i in range(n):
#             torch.cuda.empty_cache()
#             do_eval(
#                 model,
#                 bs,
#                 crop_size,
#                 im=im
#             )
#
#     torch.cuda.synchronize()
#
#     end = time.time()
#     return (bs * n) / (end - start)

import time
def do_eval_step(
    model,
    variant=None,
    im_sh=None,
    bs=None,
    im=None,
):
    if im is None:
        torch.cuda.empty_cache()
        im = torch.randn(bs, 3, *im_sh).cuda(ptu.device)
    #print("im",im.shape)
    im_meta = dict(flip=False)

    if variant is None:
        torch.cuda.synchronize()
        start = time.time()

        logits = model.forward(im)

        seg_map = logits.argmax(0)

        torch.cuda.synchronize()
        end = time.time()
    else:
        torch.cuda.synchronize()
        start = time.time()

        logits = inference(
            model,
            [im[0].unsqueeze(0)],
            [im_meta],
            ori_shape=im.shape[2:4],
            window_size=variant["inference_kwargs"]["window_size"],
            window_stride=variant["inference_kwargs"]["window_stride"],
            batch_size=2,
        )

        for i in range(1, len(im)):
            logits += inference(
                model,
                [im[i].unsqueeze(0)],
                [im_meta],
                ori_shape=im.shape[2:4],
                window_size=variant["inference_kwargs"]["window_size"],
                window_stride=variant["inference_kwargs"]["window_stride"],
                batch_size=2,
            )
        logits /= len(im)

        seg_map = logits.argmax(0)

        torch.cuda.synchronize()
        end = time.time()

    torch.cuda.synchronize()

    return (end - start)


def get_max_batchsize_eval(
    model,
    variant,
    im_sh=(1024,2048),
):
    model.eval()

    with torch.no_grad():
        cur_bs = 2
        while True:
            try:
                print("cur_bs =", cur_bs)
                do_eval_step(
                    model,
                    variant,
                    im_sh,
                    bs=cur_bs
                )

                cur_bs *= 2
            except RuntimeError as err:
                print("!!! err =", err)
                top_bs = cur_bs
                bottom_bs = cur_bs // 2
                break

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        while (top_bs - bottom_bs) > 1:
            mid_bs = (top_bs + bottom_bs) // 2
            print("mid_bs =", mid_bs)
            try:
                do_eval_step(
                    model,
                    variant,
                    im_sh,
                    bs=mid_bs
                )
                bottom_bs = mid_bs
            except RuntimeError:
                top_bs = mid_bs

        print("bottom =", bottom_bs)
        print("top =", top_bs)
        try:
            do_eval_step(
                model,
                variant,
                im_sh,
                bs=top_bs
            )
            max_bs = top_bs
        except RuntimeError as err:
            print("!!! err =", err)
            max_bs = bottom_bs

    return max_bs

def measure_speed_eval_fps(model, variant, bs, im_sh=(1024,2048), n=30):
    import torch
    import time

    model.eval()

    warm_up = 10
    torch.cuda.empty_cache()
    im = torch.randn(bs, 3, *im_sh).cuda(ptu.device)

    with torch.no_grad():
        for i in range(warm_up):
            #print("i",i)
            do_eval_step(
                model,
                variant,
                im=im,
            )

    tot_time = 0
    with torch.no_grad():
        for _ in range(n):
            tot_time += do_eval_step(
                model,
                variant,
                im=im,
            )

    return (bs * n) / tot_time


def measure_speed_eval_wrt_inputsize(
        model,
        variant,
        max_im_sh=(1024, 2048),
        n=100):
    im_sh = (64, 128)
    max_im_h, max_im_w = max_im_sh
    #double_dim = lambda shape_tup: tuple(2 * d for d in shape_tup)
    add_dim = lambda shape_tup: (shape_tup[0]+16, shape_tup[1]+32)

    print("im_sh FPS")
    while im_sh[0] < max_im_h:
        speed_result = measure_speed_eval_fps(
            model,
            variant,
            bs=1,
            im_sh=im_sh,
            n=n)
        print(im_sh, speed_result)

        im_sh = add_dim(im_sh) #double_dim(im_sh)

    speed_result = measure_speed_eval_fps(
        model,
        variant,
        bs=1,
        im_sh=max_im_sh,
        n=n)
    print(max_im_sh, speed_result)

def get_max_inputsize_eval(
        model,
        variant,
        init_im_sh=(64, 128)):
    model.eval()

    double_dim = lambda shape_tup: tuple(2 * d for d in shape_tup)
    half_dim = lambda shape_tup: tuple(d // 2 for d in shape_tup)

    with torch.no_grad():
        cur_im_sh = init_im_sh
        while True:
            try:
                print("cur_im_sh =", cur_im_sh)
                do_eval_step(
                    model,
                    variant,
                    im_sh=cur_im_sh,
                    bs=1,
                )

                cur_im_sh = double_dim(cur_im_sh)
            except RuntimeError as err:
                print("!!! err =", err)
                top_im_sh = cur_im_sh
                bottom_im_sh = half_dim(cur_im_sh)
                break

        print("bottom =", bottom_im_sh)
        print("top =", top_im_sh)
        while (top_im_sh[0] - bottom_im_sh[0]) > 1:
            mid_im_sh = half_dim(tuple(d1 + d2 for d1, d2 in zip(top_im_sh, bottom_im_sh)))
            print("mid_im_sh =", mid_im_sh)
            try:
                do_eval_step(
                    model,
                    variant,
                    im_sh=mid_im_sh,
                    bs=1,
                )
                bottom_im_sh = mid_im_sh
            except RuntimeError:
                top_im_sh = mid_im_sh

        print("bottom =", bottom_im_sh)
        print("top =", top_im_sh)
        try:
            do_eval_step(
                model,
                variant,
                im_sh=top_im_sh,
                bs=1,
            )
            max_im_sh = top_im_sh
        except RuntimeError as err:
            print("!!! err =", err)
            max_im_sh = bottom_im_sh

    return max_im_sh



def do_train_step(
    model,
    criterion,
    optimizer,
    amp_autocast,
    loss_scaler,
    bs=None,
    crop_size=768, #1024,
    num_classes=19,
    im=None,
    seg_gt=None,
    record_memory=False
):
    #print("crop cls",crop_size,num_classes)
    #print("N CLS",model.n_cls,num_classes,"CROPSIZE",crop_size)#,im.shape,seg_gt.shape)
    if im is None:
        #torch.cuda.empty_cache()
        im = torch.randn(bs, 3, crop_size, crop_size).to(ptu.device)
        seg_gt = torch.randint(low=0, high=num_classes - 1, size=(bs, crop_size, crop_size)).to(ptu.device)

    optimizer.zero_grad()
    with amp_autocast():
        #print("im===",im.shape,im.dtype)
        seg_pred = model.forward(im)
        loss = criterion(seg_pred, seg_gt)

    if loss_scaler is not None:
        loss_scaler(
            loss,
            optimizer,
            parameters=model.parameters(),
        )
    else:
        loss.backward()
        mem_bwd = torch.cuda.max_memory_allocated(ptu.device)
        #print("mem bwd",mem_bwd)
        torch.cuda.reset_max_memory_allocated(ptu.device)
        #print("max 2",torch.cuda.max_memory_allocated(ptu.device))
        optimizer.step()
        #print("max 3",torch.cuda.max_memory_allocated(ptu.device))


    del seg_pred
    del loss

    torch.cuda.synchronize()

    if record_memory:
        max_mem = torch.cuda.max_memory_allocated(ptu.device)
        return max_mem,(mem_bwd-max_mem)/bs


def get_max_batchsize_train(
    model,
    optimizer,
    amp_autocast,
    loss_scaler,
    crop_size=512,
):
    model.train()

    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)

    cur_bs = 2
    while True:
        try:
            print("cur_bs =", cur_bs)
            do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=cur_bs,
            )

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =", err)
            top_bs = cur_bs
            bottom_bs = cur_bs // 2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs + bottom_bs) // 2
        print("mid_bs =", mid_bs)
        try:
            do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=mid_bs,
            )
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        do_train_step(
            model,
            criterion,
            optimizer,
            amp_autocast,
            loss_scaler,
            bs=top_bs,
        )
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs


def measure_speed_train_fps(model, bs,
    optimizer,
    amp_autocast,
    loss_scaler,
    crop_size=768,#512,
    num_classes=19,#60,
    n=30):

    import torch
    import time

    model.train()

    warm_up = 10

    ###############
    torch.cuda.empty_cache()
    im = torch.randn(bs, 3, crop_size, crop_size).to(ptu.device)
    seg_gt = torch.randint(low=0, high=num_classes - 1, size=(bs, crop_size, crop_size)).to(ptu.device)
    ###############

    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)


    torch.cuda.synchronize()
    for i in range(warm_up):
        torch.cuda.empty_cache()
        #print("i",i)
        do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                im=im,
                seg_gt=seg_gt
        )

    start = time.time()

    for i in range(n):
        torch.cuda.empty_cache()
        #print("ii",i)
        do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                im=im,
                seg_gt=seg_gt
        )

    torch.cuda.synchronize()

    end = time.time()
    return (bs * n) / (end - start)


def get_max_cropsize_train(
    model,
    optimizer,
    amp_autocast,
    loss_scaler,
    init_crop_size=64,
):
    model.train()

    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)

    cur_crop_size = init_crop_size
    while True:
        try:
            print("cur_crop_size =", cur_crop_size)
            do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=1,
                crop_size=cur_crop_size,
            )

            cur_crop_size *= 2
        except RuntimeError as err:
            print("!!! err =", err)
            top_crop_size = cur_crop_size
            bottom_crop_size = cur_crop_size // 2
            break

    print("bottom =", bottom_crop_size)
    print("top =", top_crop_size)
    while (top_crop_size - bottom_crop_size) > 1:
        mid_crop_size = (top_crop_size + bottom_crop_size) // 2
        print("mid_crop_size =", mid_crop_size)
        try:
            do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=1,
                crop_size=mid_crop_size,
            )
            bottom_crop_size = mid_crop_size
        except RuntimeError:
            top_crop_size = mid_crop_size

    print("bottom =", bottom_crop_size)
    print("top =", top_crop_size)
    try:
        do_train_step(
            model,
            criterion,
            optimizer,
            amp_autocast,
            loss_scaler,
            bs=1,
            crop_size=top_crop_size,
        )
        max_crop_size = top_crop_size
    except RuntimeError as err:
        print("!!! err =", err)
        max_crop_size = bottom_crop_size

    return max_crop_size

def measure_speed_train_wrt_cropsize(
        model,
        optimizer,
        amp_autocast,
        loss_scaler,
        max_crop_size=1024,
        n=100):
    crop_size = 64

    print("crop_size FPS")
    while crop_size < max_crop_size:
        speed_result = measure_speed_train_fps(
            model=model,
            bs=1,
            optimizer=optimizer,
            amp_autocast=amp_autocast,
            loss_scaler=loss_scaler,
            crop_size=crop_size,
            n=n)
        print(crop_size, speed_result)

        crop_size += 16 #*= 2

    speed_result = measure_speed_train_fps(
        model=model,
        bs=1,
        optimizer=optimizer,
        amp_autocast=amp_autocast,
        loss_scaler=loss_scaler,
        crop_size=max_crop_size,
        n=n)
    print(max_crop_size, speed_result)


def measure_memory_train_wrt_cropsize(
        model,
        optimizer,
        amp_autocast,
        loss_scaler,
        max_crop_size=1024,
        n=10,
        bs=1):
    crop_size = 64

    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)
    GB = 1024.0 * 1024.0 * 1024.0

    print("crop_size params+moments activations = params+moments(GB) activations(GB)")
    while crop_size < max_crop_size:
        for i in range(n):
            params_moments_mem, activations_mem = do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=bs,
                crop_size=crop_size,
                record_memory=True
            )

        print("{} {} {} = {} {}".format(crop_size, params_moments_mem, activations_mem,
                                                  params_moments_mem / GB, activations_mem / GB))

        crop_size += 16 #*= 2

    for i in range(n):
        params_moments_mem, activations_mem = do_train_step(
            model,
            criterion,
            optimizer,
            amp_autocast,
            loss_scaler,
            bs=bs,
            crop_size=max_crop_size,
            record_memory=True
        )

    print("{} {} {} =  {} {}".format(max_crop_size, params_moments_mem, activations_mem,
                                              params_moments_mem / GB, activations_mem / GB))

# LEO - END
