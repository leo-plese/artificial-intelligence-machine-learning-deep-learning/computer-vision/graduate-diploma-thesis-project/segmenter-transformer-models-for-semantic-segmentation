import sys
from pathlib import Path
import yaml
import json
import numpy as np
import torch
import click
import argparse
from torch.nn.parallel import DistributedDataParallel as DDP

from segm.data.utils import IGNORE_LABEL


from segm.utils import distributed
import segm.utils.torch as ptu
from segm import config

from segm.model.factory import create_segmenter
from segm.optim.factory import create_optimizer, create_scheduler
from segm.data.factory import create_dataset
from segm.model.utils import num_params

from timm.utils import NativeScaler
from contextlib import suppress

from segm.utils.distributed import sync_model
from segm.engine import train_one_epoch, evaluate
from segm.engine import get_max_batchsize_train, measure_speed_train_fps


def measure_macs_params(model):
    print("model.training", model.training)
    from thop import profile
    # from torchvision.models import resnet101
    # model = resnet101().to(ptu.device)
    model.eval()
    print("tr=", model.training)
    inp = torch.randn(1, 3, 1024, 2048).to(ptu.device)
    macs, params = profile(model, inputs=(inp,))

    return macs, params


@click.command(help="")
@click.option("--log-dir", type=str, help="logging directory")
@click.option("--dataset", type=str)
@click.option("--get-max-bs-train", default=False, is_flag=True)
@click.option("--measure-speed-train", default=False, is_flag=True)
@click.option("--measure-speed-train-batchsize", default=None, type=int)
@click.option("--measure-train-duration", default=False, is_flag=True)
@click.option("--get-max-cropsize-train", default=False, is_flag=True)
@click.option("--measure-speed-train-wrt-cropsize", default=False, is_flag=True)
@click.option("--measure-speed-train-wrt-cropsize-maxcropsize", type=int, default=None)
@click.option("--measure-speed-train-wrt-cropsize-iters", type=int, default=100)
@click.option("--measure-memory-train-wrt-cropsize", default=False, is_flag=True)
@click.option("--measure-memory-train-wrt-cropsize-maxcropsize", type=int, default=None)
@click.option("--measure-memory-train-wrt-cropsize-iters", type=int, default=10)
@click.option("--im-size", default=None, type=int, help="dataset resize size")
@click.option("--crop-size", default=None, type=int)
@click.option("--window-size", default=None, type=int)
@click.option("--window-stride", default=None, type=int)
@click.option("--backbone", default="", type=str)
@click.option("--decoder", default="", type=str)
@click.option("--optimizer", default="sgd", type=str)
@click.option("--scheduler", default="polynomial", type=str)
@click.option("--weight-decay", default=0.0, type=float)
@click.option("--dropout", default=0.0, type=float)
@click.option("--drop-path", default=0.1, type=float)
@click.option("--batch-size", default=None, type=int)
@click.option("--epochs", default=None, type=int)
@click.option("-lr", "--learning-rate", default=None, type=float)
@click.option("--normalization", default=None, type=str)
@click.option("--eval-freq", default=None, type=int)
@click.option("--amp/--no-amp", default=False, is_flag=True)
@click.option("--resume/--no-resume", default=True, is_flag=True)
@click.option("--log-memory", default=False, is_flag=True)
@click.option("--batch-size-for-memory", default=1, type=int)
@click.option("--measure-macs", default=False, is_flag=True)
def main(
    log_dir,
    dataset,
    get_max_bs_train,
    measure_speed_train,
    measure_speed_train_batchsize,
    measure_train_duration,
    get_max_cropsize_train,
    measure_speed_train_wrt_cropsize,
    measure_speed_train_wrt_cropsize_maxcropsize,
    measure_speed_train_wrt_cropsize_iters,
    measure_memory_train_wrt_cropsize,
    measure_memory_train_wrt_cropsize_maxcropsize,
    measure_memory_train_wrt_cropsize_iters,
    im_size,
    crop_size,
    window_size,
    window_stride,
    backbone,
    decoder,
    optimizer,
    scheduler,
    weight_decay,
    dropout,
    drop_path,
    batch_size,
    epochs,
    learning_rate,
    normalization,
    eval_freq,
    amp,
    resume,
    log_memory,
    batch_size_for_memory,
    measure_macs,
):
    # start distributed mode
    ptu.set_gpu_mode(True)
    distributed.init_process()

    # LEO - ADD
    print("GPU ID =", ptu.gpu_id)

    # set up configuration
    cfg = config.load_config()
    model_cfg = cfg["model"][backbone]
    dataset_cfg = cfg["dataset"][dataset]
    if "mask_transformer" in decoder:
        decoder_cfg = cfg["decoder"]["mask_transformer"]
    else:
        decoder_cfg = cfg["decoder"][decoder]

    # model config
    if not im_size:
        im_size = dataset_cfg["im_size"]
    if not crop_size:
        crop_size = dataset_cfg.get("crop_size", im_size)
    if not window_size:
        window_size = dataset_cfg.get("window_size", im_size)
    if not window_stride:
        window_stride = dataset_cfg.get("window_stride", im_size)

    model_cfg["image_size"] = (crop_size, crop_size)
    model_cfg["backbone"] = backbone
    model_cfg["dropout"] = dropout
    model_cfg["drop_path_rate"] = drop_path
    decoder_cfg["name"] = decoder
    model_cfg["decoder"] = decoder_cfg

    # dataset config
    world_batch_size = dataset_cfg["batch_size"]
    num_epochs = dataset_cfg["epochs"]
    lr = dataset_cfg["learning_rate"]
    if batch_size:
        world_batch_size = batch_size
    if epochs:
        num_epochs = epochs
    if learning_rate:
        lr = learning_rate
    if eval_freq is None:
        eval_freq = dataset_cfg.get("eval_freq", 1)

    if normalization:
        model_cfg["normalization"] = normalization

    # experiment config
    batch_size = world_batch_size // ptu.world_size
    variant = dict(
        world_batch_size=world_batch_size,
        version="normal",
        resume=resume,
        dataset_kwargs=dict(
            dataset=dataset,
            image_size=im_size,
            crop_size=crop_size,
            batch_size=batch_size,
            normalization=model_cfg["normalization"],
            split="train",
            num_workers=10,
        ),
        algorithm_kwargs=dict(
            batch_size=batch_size,
            start_epoch=0,
            num_epochs=num_epochs,
            eval_freq=eval_freq,
        ),
        optimizer_kwargs=dict(
            opt=optimizer,
            lr=lr,
            weight_decay=weight_decay,
            momentum=0.9,
            clip_grad=None,
            sched=scheduler,
            epochs=num_epochs,
            min_lr=1e-5,
            poly_power=0.9,
            poly_step_size=1,
        ),
        net_kwargs=model_cfg,
        amp=amp,
        log_dir=log_dir,
        inference_kwargs=dict(
            im_size=im_size,
            window_size=window_size,
            window_stride=window_stride,
        ),
    )

    log_dir = Path(log_dir)
    log_dir.mkdir(parents=True, exist_ok=True)
    checkpoint_path = log_dir / "checkpoint.pth"

    # dataset
    dataset_kwargs = variant["dataset_kwargs"]

    if get_max_bs_train or measure_speed_train or log_memory or measure_train_duration or measure_macs or get_max_cropsize_train or measure_speed_train_wrt_cropsize or measure_memory_train_wrt_cropsize:
        # model
        net_kwargs = variant["net_kwargs"]
        net_kwargs["n_cls"] = 19 #60 #n_cls    # TODO - not hardcode
        model = create_segmenter(net_kwargs)
        model.to(ptu.device)

        # optimizer
        optimizer_kwargs = variant["optimizer_kwargs"]
        optimizer_kwargs["iter_warmup"] = 0.0
        opt_args = argparse.Namespace()
        opt_vars = vars(opt_args)
        for k, v in optimizer_kwargs.items():
            opt_vars[k] = v
        optimizer = create_optimizer(opt_args, model)

        if ptu.distributed:
            model = DDP(model, device_ids=[ptu.device], find_unused_parameters=True)

        model_without_ddp = model
        if hasattr(model, "module"):
            model_without_ddp = model.module

        amp_autocast = suppress
        loss_scaler = None
        if amp:
            amp_autocast = torch.cuda.amp.autocast
            loss_scaler = NativeScaler()


        if get_max_bs_train:
            max_bs_result = get_max_batchsize_train(
                model,
                optimizer,
                amp_autocast,
                loss_scaler,
                crop_size=768,
            )
            print("MAX BS =", max_bs_result)
            return
        elif measure_speed_train:
            speed_result = measure_speed_train_fps(
            model,
            measure_speed_train_batchsize,
            optimizer,
            amp_autocast,
            loss_scaler,
            crop_size=768,
            num_classes=19
            )
            print("FPS = ", speed_result)
            return
        elif log_memory:
            from segm.engine import do_train_step

            model.train()
            print("MODEL DEV", next(model.parameters()).device)
            print()

            mem_alloc_before_train = torch.cuda.memory_allocated(ptu.device)
            print("MEMORY allocated before TRAIN:", mem_alloc_before_train)

            criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_LABEL)

            for i in range(10):
                params_moments_mem, activations_mem= do_train_step(
                model,
                criterion,
                optimizer,
                amp_autocast,
                loss_scaler,
                bs=int(batch_size_for_memory),
                record_memory=True
                )

            print("MEMORY:")
            print("mem alloc before train:", mem_alloc_before_train)
            GB = 1024.0 * 1024.0 * 1024.0
            print("MEMORY REQUIREMENTS FOR TRAIN = (PARAMS + MOMENTS, ACTIVATIONS) = ({}, {}) =  ({} GB, {} GB)".format(params_moments_mem, activations_mem, params_moments_mem / GB, activations_mem / GB))

            return
        elif measure_train_duration:
            from segm.engine import train_one_epoch_duration
            import time

            model.train()
            print("MODEL DEV", next(model.parameters()).device)
            print()

            data_loader = create_dataset(dataset_kwargs)

            epoch_times = []
            print("BATCH_SIZE===", batch_size)
            print("*************** NUM EPOCHS =", num_epochs, "***************")
            for epoch in range(num_epochs):
                torch.cuda.synchronize()
                start = time.time()

                # train for one epoch
                train_one_epoch_duration(
                    model,
                    data_loader,
                    optimizer,
                    amp_autocast,
                    loss_scaler,
                )

                torch.cuda.synchronize()
                end = time.time()

                epoch_time = (end - start)
                print(">>>>>>>>>>>>>>> TIME =", epoch_time, ">>>>>>>>>>>>>>>")

                epoch_times.append(epoch_time)

            avg_epoch_time = 0.0
            for i in range(num_epochs):
                print("epoch {} : {}".format(i, epoch_times[i]))
                avg_epoch_time += epoch_times[i]
            avg_epoch_time /= num_epochs
            print(">>>>>>>>>>>>>> AVERAGE EPOCH TIME =", avg_epoch_time, ">>>>>>>>>>>>>>")
            return
        elif measure_macs:
            macs, params = measure_macs_params(model)

            print("MACS", macs)
            print("PARAMS", params)
            return
        elif get_max_cropsize_train:
            from segm.engine import get_max_cropsize_train
            max_cropsize_result = get_max_cropsize_train(
                model,
                optimizer,
                amp_autocast,
                loss_scaler,
            )
            print("MAX CROPSIZE =", max_cropsize_result)
            return
        elif measure_speed_train_wrt_cropsize:
            from segm.engine import measure_speed_train_wrt_cropsize

            measure_speed_train_wrt_cropsize(model, optimizer, amp_autocast, loss_scaler, max_crop_size=measure_speed_train_wrt_cropsize_maxcropsize,
                                             n=measure_speed_train_wrt_cropsize_iters)
            return
        elif measure_memory_train_wrt_cropsize:
            from segm.engine import measure_memory_train_wrt_cropsize

            measure_memory_train_wrt_cropsize(model, optimizer, amp_autocast, loss_scaler, max_crop_size=measure_memory_train_wrt_cropsize_maxcropsize,
                                             n=measure_memory_train_wrt_cropsize_iters, bs=batch_size_for_memory)
            return



    train_loader = create_dataset(dataset_kwargs)


    val_kwargs = dataset_kwargs.copy()
    val_kwargs["split"] = "val"
    val_kwargs["batch_size"] = 1
    val_kwargs["crop"] = False
    val_loader = create_dataset(val_kwargs)
    n_cls = train_loader.unwrapped.n_cls

    # model
    net_kwargs = variant["net_kwargs"]
    net_kwargs["n_cls"] = n_cls
    model = create_segmenter(net_kwargs)
    model.to(ptu.device)

    # optimizer
    optimizer_kwargs = variant["optimizer_kwargs"]
    optimizer_kwargs["iter_max"] = len(train_loader) * optimizer_kwargs["epochs"]
    optimizer_kwargs["iter_warmup"] = 0.0
    opt_args = argparse.Namespace()
    opt_vars = vars(opt_args)
    for k, v in optimizer_kwargs.items():
        opt_vars[k] = v
    optimizer = create_optimizer(opt_args, model)
    lr_scheduler = create_scheduler(opt_args, optimizer)
    num_iterations = 0
    amp_autocast = suppress
    loss_scaler = None
    if amp:
        amp_autocast = torch.cuda.amp.autocast
        loss_scaler = NativeScaler()

    # resume
    if resume and checkpoint_path.exists():
        print(f"Resuming training from checkpoint: {checkpoint_path}")
        checkpoint = torch.load(checkpoint_path, map_location="cpu")
        model.load_state_dict(checkpoint["model"])
        optimizer.load_state_dict(checkpoint["optimizer"])
        if loss_scaler and "loss_scaler" in checkpoint:
            loss_scaler.load_state_dict(checkpoint["loss_scaler"])
        lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])
        variant["algorithm_kwargs"]["start_epoch"] = checkpoint["epoch"] + 1
    else:
        sync_model(log_dir, model)

    if ptu.distributed:
        model = DDP(model, device_ids=[ptu.device], find_unused_parameters=True)

    # save config
    variant_str = yaml.dump(variant)
    print(f"Configuration:\n{variant_str}")
    variant["net_kwargs"] = net_kwargs
    variant["dataset_kwargs"] = dataset_kwargs
    log_dir.mkdir(parents=True, exist_ok=True)
    with open(log_dir / "variant.yml", "w") as f:
        f.write(variant_str)

    # train
    start_epoch = variant["algorithm_kwargs"]["start_epoch"]
    num_epochs = variant["algorithm_kwargs"]["num_epochs"]
    eval_freq = variant["algorithm_kwargs"]["eval_freq"]

    model_without_ddp = model
    if hasattr(model, "module"):
        model_without_ddp = model.module

    val_seg_gt = val_loader.dataset.get_gt_seg_maps()

    print(f"Train dataset length: {len(train_loader.dataset)}")
    print(f"Val dataset length: {len(val_loader.dataset)}")
    print(f"Encoder parameters: {num_params(model_without_ddp.encoder)}")
    print(f"Decoder parameters: {num_params(model_without_ddp.decoder)}")

    print("MEMORY allocated before TRAIN:", torch.cuda.memory_allocated(ptu.device))

    print("BATCH_SIZE===", batch_size)
    print("*************** NUM EPOCHS =", num_epochs, "***************")
    for epoch in range(start_epoch, num_epochs):
        # train for one epoch
        train_logger = train_one_epoch(
            model,
            train_loader,
            optimizer,
            lr_scheduler,
            epoch,
            amp_autocast,
            loss_scaler
        )

        # COMMENT - begin for Seg-L
        # save checkpoint
        if ptu.dist_rank == 0:
            snapshot = dict(
                model=model_without_ddp.state_dict(),
                optimizer=optimizer.state_dict(),
                n_cls=model_without_ddp.n_cls,
                lr_scheduler=lr_scheduler.state_dict(),
            )
            if loss_scaler is not None:
                snapshot["loss_scaler"] = loss_scaler.state_dict()
            snapshot["epoch"] = epoch
            torch.save(snapshot, checkpoint_path)
        # COMMENT - end for Seg-L

        # evaluate
        eval_epoch = epoch % eval_freq == 0 or epoch == num_epochs - 1
        if eval_epoch:
            eval_logger = evaluate(
                model,
                val_loader,
                val_seg_gt,
                window_size,
                window_stride,
                amp_autocast,
            )
            print(f"Stats [{epoch}]:", eval_logger, flush=True)
            print("")

        # log stats
        if ptu.dist_rank == 0:
            train_stats = {
                k: meter.global_avg for k, meter in train_logger.meters.items()
            }
            val_stats = {}
            if eval_epoch:
                val_stats = {
                    k: meter.global_avg for k, meter in eval_logger.meters.items()
                }

            log_stats = {
                **{f"train_{k}": v for k, v in train_stats.items()},
                **{f"val_{k}": v for k, v in val_stats.items()},
                "epoch": epoch,
                "num_updates": (epoch + 1) * len(train_loader),
            }

            with open(log_dir / "log.txt", "a") as f:
                f.write(json.dumps(log_stats) + "\n")


    distributed.barrier()
    distributed.destroy_process()
    sys.exit(1)


if __name__ == "__main__":
    main()
