import click
from tqdm import tqdm
from pathlib import Path
from PIL import Image
import numpy as np
import torchvision.transforms.functional as F

import segm.utils.torch as ptu
import os

from segm.data.utils import STATS
from segm.data.cityscapes import CITYSCAPES_CATS_PATH
from segm.data.utils import dataset_cat_description, seg_to_rgb

from segm.model.factory import load_model
from segm.model.utils import inference
from segm.engine import get_max_batchsize_eval, measure_speed_eval_fps

@click.command()
@click.option("--model-path", type=str)
@click.option("--input-dir", "-i", type=str, help="folder with input images")
@click.option("--output-dir", "-o", type=str, help="folder with output images")
@click.option("--gpu/--cpu", default=True, is_flag=True)
@click.option("--get-max-bs-eval", default=False, is_flag=True)
@click.option("--measure-speed-eval", default=False, is_flag=True)
@click.option("--measure-speed-eval-batchsize", default=None, type=int)
@click.option("--get-max-inputsize-eval", default=False, is_flag=True)
@click.option("--measure-speed-eval-wrt-inputsize", default=False, is_flag=True)  # dim h - from im_sh = (h, w); w = 2*h
@click.option("--measure-speed-eval-wrt-inputsize-maxinputsize", type=int, default=None)
@click.option("--measure-speed-eval-wrt-inputsize-iters", type=int, default=100)
@click.option("--without-sliding-window", default=False, is_flag=True)

def main(model_path, input_dir, output_dir, gpu,
    get_max_bs_eval,
    measure_speed_eval,
    measure_speed_eval_batchsize,
    get_max_inputsize_eval,
    measure_speed_eval_wrt_inputsize,
    measure_speed_eval_wrt_inputsize_maxinputsize,
    measure_speed_eval_wrt_inputsize_iters,
    without_sliding_window,):
    ptu.set_gpu_mode(gpu)

    model, variant = load_model(model_path)
    model.to(ptu.device)

    if get_max_bs_eval:
        variant = (None if without_sliding_window else variant)
        max_bs_result = get_max_batchsize_eval(model, variant)
        print("MAX BS =", max_bs_result)
        return
    elif measure_speed_eval:
        variant = (None if without_sliding_window else variant)
        speed_result = measure_speed_eval_fps(model, variant, measure_speed_eval_batchsize)
        print("FPS = ", speed_result)
        return
    elif get_max_inputsize_eval:
        from segm.engine import get_max_inputsize_eval

        max_inputsize_result = get_max_inputsize_eval(model, variant=None)
        print("MAX INPUTSIZE =", max_inputsize_result)
        return
    elif measure_speed_eval_wrt_inputsize:
        from segm.engine import measure_speed_eval_wrt_inputsize

        max_im_h, max_im_w = measure_speed_eval_wrt_inputsize_maxinputsize, 2*measure_speed_eval_wrt_inputsize_maxinputsize
        measure_speed_eval_wrt_inputsize(model, variant=None, max_im_sh=(max_im_h, max_im_w), n=measure_speed_eval_wrt_inputsize_iters)
        return


    normalization_name = variant["dataset_kwargs"]["normalization"]
    normalization = STATS[normalization_name]
    cat_names, cat_colors = dataset_cat_description(CITYSCAPES_CATS_PATH)

    input_dir = Path(input_dir)
    output_dir = Path(output_dir)
    output_dir.mkdir(exist_ok=True)

    list_dir = list(input_dir.glob('**/*')) #iterdir())
    #list_dir = list_dir[:20]
    print("............", len(list_dir), list_dir[0])
    for filename in tqdm(list_dir, ncols=80):
        print("filename======",filename)
        if os.path.isdir(filename):
            continue
        pil_im = Image.open(filename).copy()
        im = F.pil_to_tensor(pil_im).float() / 255
        im = F.normalize(im, normalization["mean"], normalization["std"])
        im = im.to(ptu.device).unsqueeze(0)

        im_meta = dict(flip=False)
        logits = inference(
            model,
            [im],
            [im_meta],
            ori_shape=im.shape[2:4],
            window_size=variant["inference_kwargs"]["window_size"],
            window_stride=variant["inference_kwargs"]["window_stride"],
            batch_size=2,
        )
        seg_map = logits.argmax(0, keepdim=True)
        seg_rgb = seg_to_rgb(seg_map, cat_colors)
        seg_rgb = (255 * seg_rgb.cpu().numpy()).astype(np.uint8)
        pil_seg = Image.fromarray(seg_rgb[0])

        pil_blend = pil_seg.convert("RGB") #Image.blend(pil_im, pil_seg, 0.5).convert("RGB")
        pil_blend.save(output_dir / filename.name)


if __name__ == "__main__":
    main()
